C++ API
-------

.. toctree::
   :maxdepth: 4

   BasePlasticity
   BCM
   Hopfield
   Activations
