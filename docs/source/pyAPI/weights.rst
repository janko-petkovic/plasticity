Weights initialization
----------------------

.. automodule:: model.weights
   :members:
   :undoc-members:
   :show-inheritance:
