Hopfield
--------

.. automodule:: model.hopfield
   :members:
   :show-inheritance:
   :inherited-members:
