Theory
======

The brief documentation about the mathematical background of the BCM model can be found here_.

.. _here: http://www.scholarpedia.org/article/BCM_theory
