#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import utilities

from .__version__ import __version__

__author__  = ['Nico Curti', 'SimoneGasperini', 'Mattia Ceccarelli']
__email__ = ['nico.curit2@unibo.it', 'simone.gasperini2@studio.unibo.it', 'mattia.ceccarelli5@unibo.it']
