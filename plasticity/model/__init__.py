#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import utilities

from .bcm import BCM
from .hopfield import Hopfield

__author__  = ['Nico Curti', 'SimoneGasperini']
__email__ = ['nico.curit2@unibo.it', 'simone.gasperini2@studio.unibo.it']
